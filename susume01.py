from random import choice



breakfastmenu = ['五穀飯','ポテトサラダ','お粥','抹茶ケーキ','クリームケーキ','サバ定食','バームクーヘンとコーヒー']
launchmenu = ['ローストチキン','天婦羅','マーポートーフ','カレーライス','炊き込みご飯','丼物','ハンバーガー']
dinnermenu = ['納豆かけご飯','カレーうどん','フライパン','寿司','ラーメン','焼きそば','唐揚げ定食']


def kekkb():
	i=input("ほかの料理にしますか？\ny or n\n")
	if i=="y":
		kekka()
	elif i=="n":
		print("ご利用ありがとうございます")

def kekka():
	
	time = input("どれにしますか？\n朝食(a)・昼食(b)・夕食(c)\n")
	
	if time=="a":
		print(choice(breakfastmenu),"がお薦めです")
		kekkb()
	elif time=="b":
		print(choice(launchmenu),"がお薦めです")
		kekkb()		
	elif time=="c":
		print(choice(dinnermenu),"がお薦めです")
		kekkb()
	else:
		print("指示通りに入力してくださいね")
		kekka()

kekka()
